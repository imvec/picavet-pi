# PicavetPi
Cámara estabilizada mecanicamente para la realización de fotografía aérea y la captura de datos ambientales utilizando cometas y globos cautivos.

### Un poco de contexto
En el I.M.V.E.C. empleamos la fotografía aerea para monitorizar espacios contaminados junto a las vecinas afectadas y para obtenerlas, utilizamos principalmente cometas en lugar de drones por diferentes motivos: rápido aprendizaje, reducido consumo energético, larguísimos tiempos de vuelo, no necesidad de licencia ni permiso de vuelo, equipación económica... Ante la necesidad de saber con exactitud a qué altura tomamos las fotografías, desde el I.M.V.E.C. decidimos en Mayo de 2022 iniciar el desarrollo de una cámara fotográfica abierta que junto a las fotografías de los espacios, también recogiese y añadiese a las imágenes la altitud y geolocalización a la que se dispararon. Durante el proceso, el equipo de desarrollo de herramientas del I.M.V.E.C. decidió añadir a la cámara la capacidad de obtener datos ambientales como temperatura humedad, presión, etc... dando a las fotografías valor añadido y permitiendo monitorizar los espacios investigados con mayor detalle.

El fruto de este proceso es el primer prototipo de la PicaPiKAP, una cámara basada en la minicomputadora Raspberry Pi, ensamblada junto a los sensore ambientales en un soporte estabilizado Picavet, un dispositivo tan antiguo como la fotografía, diseñado en el año 1912 por el inventor Francés Pierre Picavet.

# Lista de materiales
### Electrónica

- 1 RaspberryPi 3.
- 1 Tarjeta microSD clase 10 de 32gb.
- 1 Módulo de cámara Raspberry Pi V2.
- 1 Módulo gps GT-U7.
- 1 Sensor BME280 (Temperatura, humedad y presión atmosférica).
- 1 Shield de Batería Lipo de 3800mAh
- 10 cables tipo jumper

_**Coste aproximado: +/- 130€**_

### Soporte Picavet

- 1 Caja de conexiónes de 150x100x7cm.
- 4 Cáncamos de métrica 5 (M5).
- 8 Tuercas autoblocantes.
- 10 Metros de cuerda de cometa.
- 2 Mosquetones.
- 1 Aro de plástico o metal ligero.
- 50 Centímetros de alambre de 3mm.

_**Coste aproximado: +/- 30€**_

#### Materiales para el montaje

- Taladro y brocas de 4 y 20mm.
- Cinta de doble lado o velcro adhesivo.
- Cuchilla.
- Llave inglesa.
- Alicate.

# Electrónica
La PicaPiKAP está costruída alrededor de la minicomputadora Raspberry Pi y dispone de un sistema operativo RaspberryOS, un port del sistema operativo Debian. Estos son los pasos que seguiremos:
- Descarga e instalación del sistema operativo
- Descarga y ejecución del instalador de los diferentes componentes de la cámara.
- Test del programa de disparo.

#### Sistema preconfigurado
Si tan solo quieres utilizar la cámara sin muchas complicaciones te recomendamos que descargues en el siguiente enlace nuestro sistema operativo preconfigurado y listo para utilizar. Tan solo tienes que descargar la imagen de disco en este enlace, flashearla regularmente como un sistema operativo para Raspberry Pi utilizando Etcher e introducirla en tu Raspberry Pi. La cámara comenazará a disparar treinta segundos después del arranque. Después puedes continuar leyendo esta documentación para controlar tucámara y aprender cómo mejorarla.
