import time
import gpsd
import board
import digitalio
from time import sleep
from picamera import PiCamera
from datetime import datetime, timedelta
from adafruit_bme280 import basic as adafruit_bme280

spi = board.SPI()
bme_cs = digitalio.DigitalInOut(board.D5)
bme280 = adafruit_bme280.Adafruit_BME280_SPI(spi, bme_cs)

bme280.sea_level_pressure = 1013.25

gpsd.connect()

camera = PiCamera()
camera.resolution = (3280, 2464)
#camera.led = False

with open("/home/pi/picavetPI/media/datalog.csv", "a") as log:

    while True:
            tiempo = (time.strftime("%y%b%d_%H:%M:%S_"))
            packet = gpsd.get_current()
            longitud = str("%0.7f_" % packet.lon)
            latitud = str("%0.7f_" % packet.lat)
            altitudgps = str(packet.alt)
            temperatura = str("%0.1fC_" % bme280.temperature)
            humedad = str("%0.1f%%_" % bme280.relative_humidity)
            presion = str("%0.1fhPa" % bme280.pressure)
            altitud = str("%0.2fm_" % bme280.altitude)
            log.write("{0},{1},{2},{3},{4},{5},{6},{7}\n".format(time.strftime("%y%b%d_%H:%M:%S"),str("%0.7f" % packet.lon),str("%0.7f" % packet.lat),str(packet.alt),str("%0.1f" % bme280.temperature),str("%0.1f" % bme280.relative_humidity),str("%0.1f" % bme280.pressure),str("%0.2f" % bme280.altitude)))
            camera.capture("/home/pi/picavetPI/media/" + tiempo + longitud + latitud + altitud + temperatura + humedad + presion + ".jpg")
            print("Datos del GPS")
            print("Satelites: " + str(packet.sats))
            print("Longitud:" + str(packet.lon))
            print("Latitud:" + str(packet.lat))
            print("Altitud GPS:" + str(packet.alt))
            print("Datos del BME280")
            print("\nTemperatura: %0.1f C" % bme280.temperature)
            print("Humedad relativa: %0.1f %%" % bme280.relative_humidity)
            print("Presion: %0.1f hPa" % bme280.pressure)
            print("Altitud = %0.2f metros" % bme280.altitude)
            print("-----")
            time.sleep(5)

