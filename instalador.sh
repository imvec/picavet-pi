# !/bin/bash
#
# CÓMO UTILIZAR ESTE SCRIPT DE INSTALACIÓN
# Instala la última versión de Raspberry OS Lite y flashéala usando Etcher.io.
# Enchufa la Raspberry a un monitor y un teclado.
# Enciéndela.
# Entra en la carpeta de usuaria escribiendo "cd /home/pi"
# Descarga este archivo con "sudo wget https://gitlab.com/imvec/picavet-pi/-/raw/main/instalador.sh"
# Concédele permiso de ejecución con "sudo chmod +x instalador.sh"
# Ejecuta el archivo con "sudo ./instalador.sh"
#
#
# HOW TO USE THIS INSTALLATION SCRIPT
# Install latest version of Raspberry OS Lite and flash it using Etcher.io
# Plug the Raspberry to a monitor and a keyboard. 
# Boot it up.
# Go to your user directory typing "cd /home/pi"
# Get this installer file with "sudo wget https://gitlab.com/imvec/picavet-pi/-/raw/main/instalador.sh"
# Give execute permission to the file with "sudo chmod +x instalador.sh"
# Execute the file with "sudo ./instalador.sh"
#
#
 echo ""
 echo ""
 echo ""
 echo "=============================="
 echo "   Preparando la instalación  "
 echo "=============================="
sleep 2
 echo ""  
 echo "==== Actualizando repositorios y sistema ===="
 echo ""
 echo "Actualización de los programas y de los repositorios de código"
sleep 3
 echo ""
apt-get update && upgrade
 echo ""
 echo "==== Activando cámara y conexión i2C ===="
 echo ""
 echo "Se descarga un nuevo archivo de configuración de raspberry para sua activación automática"
sleep 3
 echo ""
cd /boot
rm -rf config.txt
wget https://gitlab.com/imvec/picavet-pi/-/raw/main/config.txt
 echo ""
 echo "==== Creando carpetas ===="
 echo ""
 echo "Se crea la carpera picavetPI en la ruta /home/pi y en su interior dos carpetas más: media y scripts"
 echo "En la carpeta media se almacenan las imágenes y en la carpeta scripts el script de disparo automático"
sleep 3
 echo ""
mkdir /home/pi/picavetPI
mkdir /home/pi/picavetPI/media
mkdir /home/pi/picavetPI/scripts
 echo ""
 echo "==== Adquiriendo el script de cámara ===="
 echo ""
 echo "Se descarga el script que hará que la Picavet Pi se dispare automáticamente cada vez que arranque"
sleep 3
 echo ""
cd /home/pi/picavetPI/scripts
wget https://gitlab.com/imvec/picavet-pi/-/raw/main/picavetPI.py
chmod +x picavetPI.py
 echo ""
 echo "==== Actualizando repositorios ===="
 echo ""
sleep 2
 echo ""
apt-get update
 echo ""
 echo "==== Instalando python3-pip ===="
 echo ""
sleep 3
 echo ""
sudo apt-get install python3-pip -y
 echo ""
 echo "==== Instalando el módulo picamera ===="
 echo ""
sleep 3
 echo ""
sudo pip install picamera
 echo ""
 echo "==== Instalando el módulo datetime ===="
 echo ""
sleep 3
 echo ""
sudo pip install datetime
 echo ""
 echo "==== Instalando librería Adafruit circuit python BME280 ===="
 echo ""
 sleep 3
 echo ""
sudo pip3 install adafruit-circuitpython-bme280
 echo ""
  echo "==== Instalando librería Adafruit circuit python GPS ===="
 echo ""
 sleep 3
 echo ""
sudo pip3 install adafruit-circuitpython-gps
 echo ""
 echo "==== Instalando librería PMS5003 y configurando el puerto de serie ===="
 echo ""
 sleep 3
 echo ""
sudo pip3 install pms5003
sudo raspi-config nonint do_serial 1
sudo raspi-config nonint set_config_var enable_uart 1 /boot/config.txt
 echo ""
 echo "==== Eliminando el instalador ===="
 echo ""
 echo "Sin comentario ;D"
sleep 3
 echo ""
cd /home/pi
rm -rf instalador.sh
 echo "==============================================================================="
 echo "=                                  SIGUIENTES PASOS                           ="
 echo "==============================================================================="
 echo "= Escribe: sudo crontab -e y añade esta linea al final del archivo            ="
 echo "= para que la cámara comience a disparar al iniciar la Raspberry Pi:          ="
 echo "= @reboot sudo python /home/pi/picavetPI/scripts/picavetPI.py                 ="  
 echo "= Recomendamos retrasar 5 minutos (300 segundos) el inicio del disparo para   ="
 echo "= poder levantar la cometa y que el disparo se active en el aire. En ese caso ="
 echo "= debes añadir esta línea y no la anterior: 
 echo "= @reboot sleep 300 && sudo python /home/pi/picavetPI/scripts/picavetPI.py    ="             
 echo "==============================================================================="
 echo ""
sleep 5
 echo ""
 echo "==============================================================================="
 echo "=                  ¿Dudas o problemas durante la instalación?                 ="
 echo "=             Ponte en contacto con nosotras en imvec@tutanota.com            ="
 echo "==============================================================================="
 echo ""
 echo ""
 echo ""
 echo ""
