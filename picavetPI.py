
import time
import board
import digitalio
from time import sleep
from picamera import PiCamera
from datetime import datetime, timedelta
from adafruit_bme280 import basic as adafruit_bme280

spi = board.SPI()
bme_cs = digitalio.DigitalInOut(board.D5)
bme280 = adafruit_bme280.Adafruit_BME280_SPI(spi, bme_cs)

bme280.sea_level_pressure = 1013.25

camera = PiCamera()
camera.resolution = (3280, 2464)
#camera.led = False

while True:
    tiempo = (time.strftime("%y%b%d_%H:%M:%S_"))
    altitud = str("%0.2fm_" % bme280.altitude)
    temperatura = str("%0.1fC_" % bme280.temperature)
    humedad = str("%0.1f%" % bme280.relative_humidity)
    presion = str("%0.1fhPa" % bme280.pressure)
    camera.capture("/home/pi/picavetPI/media/" + tiempo + altitud + temperatura + presion + ".jpg")
    print("\nTemperatura: %0.1f C" % bme280.temperature)
    print("Humedad relativa: %0.1f %%" % bme280.relative_humidity)
    print("Presion: %0.1f hPa" % bme280.pressure)
    print("Altitud = %0.2f metros" % bme280.altitude)
    time.sleep(5)

