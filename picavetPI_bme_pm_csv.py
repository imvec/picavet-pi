import time
import board
import digitalio
from time import sleep
from pms5003 import PMS5003
from picamera import PiCamera
from datetime import datetime, timedelta
from adafruit_bme280 import basic as adafruit_bme280

spi = board.SPI()
bme_cs = digitalio.DigitalInOut(board.D5)
bme280 = adafruit_bme280.Adafruit_BME280_SPI(spi, bme_cs)
bme280.sea_level_pressure = 1013.25

pms5003 = PMS5003(
    device='/dev/ttyAMA0',
    baudrate=9600,
    pin_enable=22,
    pin_reset=27
)

camera = PiCamera()
camera.resolution = (3280, 2464)
#camera.led = False

with open("/home/pi/picavetPI/media/datalog.csv", "a") as log:

    while True:
            data = pms5003.read()
            tiempo = (time.strftime("%y%b%d_%H:%M:%S_"))
            altitud = str("%0.2fm_" % bme280.altitude)
            temperatura = str("%0.1fC_" % bme280.temperature)
            humedad = str("%0.1f%%_" % bme280.relative_humidity)
            presion = str("%0.1fhPa_" % bme280.pressure)
            pm25 = str(data.pm_ug_per_m3(2.5))
            log.write("{0},{1},{2},{3},{4},{5},{6}\n".format(time.strftime("%y%b%d_%H:%M:%S"),str("%0.1f" % bme280.temperature),str("%0.1f" % bme280.relative_humidity),str("%0.1f" % bme280.pressure), str("%0.2f" % bme280.altitude), str>
            camera.capture("/home/pi/picavetPI/media/" + tiempo + altitud + temperatura + humedad + presion + pm25 + ".jpg")
            print("\nTemperatura: %0.1f C" % bme280.temperature)
            print("Humedad relativa: %0.1f %%" % bme280.relative_humidity)
            print("Presion: %0.1f hPa" % bme280.pressure)
            print("Altitud = %0.2f metros" % bme280.altitude)
            print(data)
            time.sleep(5)
